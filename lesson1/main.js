// Task1

let nameTown = 'Kurakhovo',
    countryTown = 'Ukraine',
    populationTown = 18782,
    footballStadium = true;


// Task2

let widthRectangle = 70,
    heightRectangle = 40;

let squareRectangle = widthRectangle * heightRectangle;
console.log(squareRectangle);


// Task3

let time = 2,
    speedOfFirst = 95,
    speedOfSecond = 114;

let distance = (speedOfFirst * speedOfSecond) * time;
console.log(distance);


// Task4

const randomNumber = Math.floor(Math.random() * 100);
console.log(randomNumber);

if(randomNumber < 20) {
  console.log('randomNumber меньше 20');
} else if (randomNumber > 50) {
  console.log('randomNumber больше 50');
} else {
  console.log('randomNumber больше 20, и меньше 50');
}


// Task5


switch(true) {
  case (randomNumber < 20):
    console.log('randomNumber меньше 20');
    break;
  case (randomNumber > 50):
    console.log('randomNumber больше 50');
    break;
  default :
    console.log('randomNumber больше 20, и меньше 50');
    break;
}
